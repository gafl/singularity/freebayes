# freebayes Singularity container
### Bionformatics package freebayes<br>
freebayes is a Bayesian genetic variant detector designed to find small polymorphisms, specifically SNPs (single-nucleotide polymorphisms), indels (insertions and deletions), MNPs (multi-nucleotide polymorphisms), and complex events (composite insertion and substitution events) smaller than the length of a short-read sequencing alignment.<br>
freebayes Version: 1.3.2<br>
[https://github.com/ekg/freebayes]

Singularity container based on the recipe: Singularity.freebayes_v1.3.2

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build freebayes_v1.3.2.sif Singularity.freebayes_v1.3.2`

### Get image help
`singularity run-help ./freebayes_v1.3.2.sif`

#### Default runscript: STAR
#### Usage:
  `freebayes_v1.3.2.sif --help`<br>
    or:<br>
  `singularity exec freebayes_v1.3.2.sif freebayes --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull freebayes_v1.3.2.sif oras://registry.forgemia.inra.fr/gafl/singularity/freebayes/freebayes:latest`


